class UnderAgeException(Exception):
    def __init__(self,arg="age can't be < 18!"):
        self.strerror = arg

def input_age(age):
   try:
       if(int(age)<=18):
           raise ZeroDivisionError  # if runs,this will break code execution
   except ValueError:
       return '\033[1;31m ValueError: Cannot convert into int\033[0m'
   else:
       return '\033[1;32m Age is saved successfully \033[0m'

print(input_age('a'))  # This will execute properly
print(input_age('19'))  # This will not execute properly