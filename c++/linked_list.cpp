#include <iostream>

using namespace std;


//+===========================================================================+
//|                          Custom Exception class                           |
//+===========================================================================+
class ListEmptyException : public exception{
    public:
    const char* message() const throw() {
        return "List is empty\n";
    }
};
//+===========================================================================+


//+===========================================================================+
//|                              Node class                                   |
//+===========================================================================+
template <class NodeType>
class Node{

    NodeType data;
    Node<NodeType>* next;

    Node(NodeType data=NULL){
        this->data = data;
        next = NULL;
    }

    // since LinkedList class is friend, it can access all data members of 
    // Node class (private members too ;) 
    template <typename ListType>
    friend class LinkedList;   
    
};
//+===========================================================================+


//+===========================================================================+
//|                          Linked List class                                |
//+===========================================================================+
template <class ListType>
class LinkedList{

    // the starting point of list 
    Node<ListType>* root;

    public:
    
    void add(ListType data){
        Node<ListType>* temp = (Node<ListType>*)malloc(sizeof(Node<ListType>));
        temp->data = data;

        if(root==NULL){
            root = temp;
        }
        else{
            Node<ListType>* ptr = root;
            while(ptr->next!=NULL){
                ptr = ptr->next;
            }
            ptr->next = temp;
        }
    }

    void print(){
        if(root==NULL){
            cout<<"Empty!"<<endl;
            return;
        }
        Node<ListType>* temp = root;
        while(temp!=NULL){
            cout<<temp->data<<" ";
            temp = temp->next;
        }
        cout<<endl;

    }

    void del(){
        try{
            if(root==NULL)
                throw ListEmptyException();
        }catch(ListEmptyException &e){
            cout<<e.message();
        }
    }

    // pre-increment operator overloading
    void operator ++(int){
        cout<<"I am overloaded!"<<endl;
    }

    // post-increment operator overloading
    void operator ++(){
        cout<<"I am overloaded!"<<endl;
    }
};
//+===========================================================================+

int  main(){

    LinkedList<float> list = LinkedList<float>();
    list.del();
    list.print();
    list.add(1.5);
    list.print();
    list.add(2.6);
    list.add(3);
    list.add(4);
    list.print();
    list++;
    ++list;
    return 0;
}