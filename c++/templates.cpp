#include <iostream>

using namespace std;

template <typename T>
class Node{
    public:
    T data;
    Node<T>* next;

    template<typename ListType>
    friend class LinkedList;
};

template <typename T>
class LinkedList{

    public:
    Node<T> *root;

    void add(T data){
        Node<T> *temp = new Node<T>;
        temp->data = data;
        temp->next = NULL;
        root = temp;
    }
};

int main(){
    LinkedList<int> list;
    list.add(1);
    cout<<list.root->data<<endl;
    return 0;
}