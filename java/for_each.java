// ctrl + alt + n to run code
class for_each{
    public static void main(String []args){
        
        // For each string loop
        for(char s : "abcdef".toCharArray() )
        System.out.println(s);

        // For each int loop
        int []intergers = {1,2,3,4,5};

        for(int i: intergers){
            System.out.println(i);
        }
    }
}