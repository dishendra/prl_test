class command_line_arguments{
    public static void main(String[] args) {
        System.out.println("Command line arguments are: ");
        for(String s: args){
            System.out.print(s+" ");
        }
        System.out.println();
        command_line_arguments.variableArgs(1,2,3,4,5,6,7,8);
    }

    static void variableArgs(int ... intergers){
        System.out.println("Type of argument: "
        +intergers.getClass().getSimpleName());
        
        for(int i: intergers){
            System.out.print(i+" ");
        }
        System.out.println();
    }

}   