import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.FileReader;

import javax.print.attribute.standard.Finishings;

public class file_read_write{
    public static void main(String[] args) throws IOException {
        FileOutputStream fout = new FileOutputStream("temp.txt");
        fout.write("Hello".getBytes());
        fout.close();

        // //////////////////////////////////////////
        // Reding char by char
        FileInputStream fin = new FileInputStream("temp.txt");
        int i;
        while((i = fin.read())!=-1){
            System.out.print((char)i);
        }
        fin.close();
        //////////////////////////////////////////

        System.out.println();

        //////////////////////////////////////////
        // Reading Line by Line
        BufferedReader br = new BufferedReader(new FileReader("temp.txt"));
        String line;
        while((line=br.readLine())!=null){
            System.out.println(line);
            line = br.readLine();
        } 
        br.close();
        //////////////////////////////////////////

        
        
    }
} 