import java.io.*;

class append_file{
    
    public static void main(String[] args) throws IOException{   
        
        File file = new File("temp.txt");

        if(!file.exists())
            file.createNewFile();

        System.out.println();
        // true signifies append mode
        FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);    
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("okay\n");
        bw.close();
    }
}