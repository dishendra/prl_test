class LinkedListEmpty extends Exception{
    public  String toString(){
        return "Linked List empty";
    }
}

class LinkedList{
    
    class Node{
        Node next = null;
        int data; 

        Node(int data){
            this.data = data;
        }
    }

    Node root = null;
    Node last = root;

    void add(int data){
        Node node = new Node(data);
        if(null==root){
            root = node;
            last = root;
        }
        else{
            last.next = node;
            last = node;
        }
    }

    int delete() throws LinkedListEmpty{
       if(null == root){
        throw new LinkedListEmpty(); 
       }
       Node temp=root;
       int return_data;
       while(temp.next!=last){
           temp = temp.next;
       }
       return_data = temp.next.data;
       temp.next = null;
       return return_data;
    }

    void print(){
        Node temp = root;
        while(null!=temp){
            System.out.print(temp.data+" ");
            temp = temp.next;
        }
        System.out.println();
    }

}

class LinkedListTest{
    public static void main(String[] args){
        LinkedList list = new LinkedList();

        try {
            list.delete();
        } catch (LinkedListEmpty e){
            e.printStackTrace();
        }

        list.print();
        list.add(1);
        list.add(2);
        list.add(3);
        list.print();
    
        try {
            System.out.println("\33[1;31mDeleted: \33[1;0m"+list.delete());
        } catch (LinkedListEmpty e){
            e.printStackTrace();
        }
        list.print();
    }
}