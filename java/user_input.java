import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class user_input{
    public static void main(String[] args) throws IOException{
        
        // ////////////////////////////////////////
        // Using Scanner class without input validation
        Scanner scan = new Scanner(System.in);
 
        System.out.print("Enter your age: ");
        int age = scan.nextInt();
        scan.nextLine();    // to consume the nextline character
        System.out.print("Enter your name: ");
        String name = scan.nextLine();

        
        
        System.out.println("Name: "+name);
        System.out.println("Age: "+age);
        // ////////////////////////////////////////

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter your age: ");
        age = Integer.parseInt(br.readLine());
        System.out.print("Enter your name: ");
        name = br.readLine();

        System.out.println("Name: "+name);
        System.out.println("Age: "+age);
        

    }
}