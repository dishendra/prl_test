#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    int data;
    struct node* next;
}node;

node* root=NULL;

void add(int data){
    node* new_node;
    new_node = (node*)malloc(sizeof(node));
    new_node->data = data;
    new_node->next = NULL;
    
    if(NULL==root){
        printf("Adding node: %d \n",new_node->data);
        root = new_node;
    }
    else{
        node* temp = root;
        while(temp->next != NULL){
            temp = temp->next;
        }
        temp->next = new_node;
    }
}

void print_list(){
    if(root==NULL){
        printf("List if empty!\n");
    }
    node* temp = root;
    while(temp!=NULL){
        printf("%d ",temp->data);
        temp = temp->next;
    }
    printf("\n");
}



int main(){
    
    print_list();
    add(1);
    add(2);
    print_list();
    
    return 0;
}   