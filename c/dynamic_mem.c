#include <stdio.h>
#include <stdlib.h>

int main(){
    printf("%s", "Enter array size: ");
    int size;
    scanf("%d",&size);

    int* array = NULL;
    array = (int *) calloc(size,sizeof(int));

    for(int i=0; i<size; i++){
        array[i] = i+1;
    }
    
    for(int i=0; i<size; i++){
        printf("%d ",array[i]);
    }
    printf("\n");
    free(array);
    return 0;
}